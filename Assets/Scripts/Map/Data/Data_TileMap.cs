
public class Data_TileMap {

    int sizeX;
    int sizeY;

    public enum TileType {
        grass,
        water,
        mountain,
    };

    TileType[,] mapData;    

    public Data_TileMap(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        mapData = new TileType[sizeX, sizeY];
    }

    public TileType GetTile(int x, int y) {
        return mapData[x, y];
    }
}