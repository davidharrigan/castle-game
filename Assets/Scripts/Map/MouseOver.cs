﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TileMap))]
public class MouseOver : MonoBehaviour {

	TileMap _tileMap;
	Vector3 _currentTileCoords;

	public Transform selectionCube;

	void Start() {
		_tileMap = GetComponent<TileMap>();
	}

	// Update is called once per frame
	void Update () {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hitInfo;
		
		Collider collider = GetComponent<Collider>();

		if (collider.Raycast(ray, out hitInfo, Mathf.Infinity)) {
			int x = Mathf.FloorToInt(hitInfo.point.x / _tileMap.tileSize);
			int z = Mathf.FloorToInt(hitInfo.point.z / _tileMap.tileSize);
			Debug.Log("Tile: " + x + ", " + z);

			_currentTileCoords = new Vector3(x, 0, z);
			selectionCube.transform.position = _currentTileCoords;
		}

	}
}
