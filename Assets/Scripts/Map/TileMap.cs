﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class TileMap : MonoBehaviour {

	public int sizeX = 100;
	public int sizeZ = 50;
	public float tileSize = 1.0f;

	public Data_TileMap tileMapData;

	// Use this for initialization
	void Start () {
		Generate();
	}

	public void Generate() {
		tileMapData = new Data_TileMap(sizeX, sizeZ);
		BuildMesh();
		BuildTexture();
	}

	void BuildTexture() {
		int texWidth = 10;
		int texHeight = 10;
		Texture2D texture = new Texture2D(texWidth, texHeight);

		for (int y=0; y < texHeight; y++) {
			for (int x=0; x < texWidth; x++) {
				Color c = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
				texture.SetPixel(x, y, c);
			}
		}

		texture.filterMode = FilterMode.Point;
		texture.Apply();

		MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
		meshRenderer.sharedMaterials[0].mainTexture = texture;
	}

	void BuildMesh() {
		int numTiles = sizeX * sizeZ;
		int numTriangles = numTiles * 2;

		int vsizeX = sizeX + 1;
		int vsizeZ = sizeZ + 1;
		int numVerts = vsizeX * vsizeZ;

		// Generate the mesh data
		Vector3[] vertices = new Vector3[numVerts];
		Vector3[] normals = new Vector3[numVerts];
		Vector2[] uv = new Vector2[numVerts];
		int[] triangles = new int[numTriangles * 3];

		for (int z=0; z < vsizeZ; z++) {
			for (int x=0; x < vsizeX; x++) {
				int idx = z * vsizeX + x;
				float height = 0f;

				vertices[idx] = new Vector3(x*tileSize, height, z*tileSize);
				normals[idx] = Vector3.up;
				uv[idx] = new Vector2((float)x / (float)sizeX, (float)z / (float)sizeZ);
			}
		}

		for (int z=0; z < sizeZ; z++) {
			for (int x=0; x < sizeX; x++) {
				int idx = (z * sizeX + x) * 6;
				int vert = z * vsizeX + x;
				triangles[idx + 0] = vert + 0;
				triangles[idx + 1] = vert + vsizeX + 0;
				triangles[idx + 2] = vert + vsizeX + 1;

				triangles[idx + 3] = vert + 0;
				triangles[idx + 4] = vert + vsizeX + 1;
				triangles[idx + 5] = vert + 1;
			}
		}

		// Create mesh and populate it with data
		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.normals = normals;
		mesh.uv = uv;

		// Assign mesh to our filter / renderer / collider
		MeshFilter meshFilter = GetComponent<MeshFilter>();
		// MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
		MeshCollider meshCollider = GetComponent<MeshCollider>();

		meshFilter.mesh = mesh;
		meshCollider.sharedMesh = mesh;
	}
	
}
